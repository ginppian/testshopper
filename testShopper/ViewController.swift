//
//  ViewController.swift
//  testShopper
//
//  Created by Gmo Ginppian on 12/20/18.
//  Copyright © 2018 gonet. All rights reserved.
//

import UIKit
import Foundation
import CoreData

class ViewController: BaseViewController {

    @IBOutlet weak var segmented: UISegmentedControl!
    @IBOutlet weak var tableView: UITableView!
    
    var url = URL(string: "http://uneabastomovil.com/services/jsonPrueba.txt")!
    var service = BasicService()

    var arrObj = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        service.delegate = self
        service.submitRequest(url: url)
    }
    
    @IBAction func segmentedAction(_ sender: UISegmentedControl) {
        arrObj.removeAll()
        switch segmented.selectedSegmentIndex {
        case 0:
            arrObj = ModelView.arrEdos()
        case 1:
            arrObj = ModelView.arrCategory()
        default:
            break;
        }
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
}

extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var tienda = String.Empty
        switch segmented.selectedSegmentIndex {
        case 0:
            let idEdo = ModelView.getIdEdoFrom(nombreEdo: arrObj[indexPath.row])
             tienda = ModelView.category(tiendaId: idEdo)
        case 1:
            let idCtgry = ModelView.getIdCtgryFrom(nombreCtgry: arrObj[indexPath.row])
            tienda = ModelView.category(tiendaId: idCtgry)
        default:
            break;
        }

        showAlert(self,
                  title: "\(arrObj[indexPath.row])",
                  message: "Tienda \(tienda) ⛺️",
                  preferredStyle: .alert,
                  actions: okAction)
    }
}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrObj.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "myCell", for: indexPath) as? BasicTableViewCell {
            cell.lbl.text = arrObj[indexPath.row]
            return cell
        }
        return UITableViewCell()
    }
}

extension ViewController: ServiciosDelegate {
    func respuestaExistosa(objecto: [String: AnyObject]) {
        // Save
        ModelView.parse(_estados: objecto)
        ModelView.parse(_tiendas: objecto)
        ModelView.parse(_categorias: objecto)
        
        arrObj = ModelView.arrEdos()
        
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    func respuestaFallida(error: Any) {
        let e = error as? NSError ?? NSError()
        print("Error inesperado: \(e.localizedDescription)")
    }
}

