//
//  BaseViewController.swift
//  testShopper
//
//  Created by Gmo Ginppian on 12/20/18.
//  Copyright © 2018 gonet. All rights reserved.
//

import Foundation
import UIKit

class BaseViewController: UIViewController {
    
    let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func showAlert(_ viewController: UIViewController,
                   title: String?,
                   message: String?,
                   preferredStyle: UIAlertController.Style,
                   actions: UIAlertAction...
        ) {
        
        let alertController = UIAlertController(title: title,
                                                message: message, preferredStyle: preferredStyle)
        
        if actions.count == 0 {
            let noAction = UIAlertAction(title: "OK",
                                         style: .default,
                                         handler: nil)
            alertController.addAction(noAction)
        } else {
            for action in actions {
                alertController.addAction(action)
            }
        }
        viewController.present(alertController,
                               animated: true,
                               completion: nil)
    }
}
