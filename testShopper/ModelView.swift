//
//  ModelView.swift
//  testShopper
//
//  Created by Gmo Ginppian on 12/20/18.
//  Copyright © 2018 gonet. All rights reserved.
//

import UIKit
import Foundation
import CoreData

extension String {
    public static let Empty = ""
}

class ModelView: NSObject {
    
    class func parse(_estados objecto: [String: AnyObject]) {
        let context = CD.share.context
        
        if let estados = objecto["estados"] as? [NSDictionary] {
            for e in estados {
                
                let id = e["id"] as? String ?? String.Empty
                let nombre = e["nombre"] as? String ?? String.Empty
                
                let estados = NSEntityDescription.entity(forEntityName: "Estados", in: context)
                let newEstado = NSManagedObject(entity: estados!, insertInto: context)
                
                newEstado.setValue(id, forKey: "id")
                newEstado.setValue(nombre, forKey: "nombre")
            }
        }
        
        do {
            try context.save()
        } catch {
            print("Failed saving")
        }
    }
    
    class func parse(_categorias objecto: [String: AnyObject]) {
        let context = CD.share.context

        if let categorias = objecto["categorias"] as? [NSDictionary] {
            for c in categorias {
                
                let id = c["id"] as? Int ?? 0
                let nombre = c["nombre"] as? String ?? String.Empty
                
                let categorias = NSEntityDescription.entity(forEntityName: "Categorias", in: context)
                let newCategoria = NSManagedObject(entity: categorias!, insertInto: context)
                
                newCategoria.setValue(id, forKey: "id")
                newCategoria.setValue(nombre, forKey: "nombre")
            }
        }
        
        do {
            try context.save()
        } catch {
            print("Failed saving")
        }
    }
    
    class func parse(_tiendas objecto: [String: AnyObject]) {
        let context = CD.share.context

        if let tiendas = objecto["tiendas"] as? [NSDictionary] {
            for t in tiendas {
                
                let id = t["id"] as? Int ?? 0
                let estado = t["estado"] as? String ?? String.Empty
                let categoria = t["categoria"] as? Int ?? 0
                let nombre = t["nombre"] as? String ?? String.Empty
                
                let tiendas = NSEntityDescription.entity(forEntityName: "Tiendas", in: context)
                let newTienda = NSManagedObject(entity: tiendas!, insertInto: context)
                
                newTienda.setValue(id, forKey: "id")
                newTienda.setValue(estado, forKey: "estado")
                newTienda.setValue(categoria, forKey: "categoria")
                newTienda.setValue(nombre, forKey: "nombre")
                
            }
        }
        
        do {
            try context.save()
        } catch {
            print("Failed saving")
        }
    }
    
    class func arrEdos() -> [String] {
        let context = CD.share.context

        var arrEdos = [String]()
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Estados")
        //request.predicate = NSPredicate(format: "age = %@", "12")
        request.returnsObjectsAsFaults = false
        do {
            let result = try context.fetch(request)
            for data in result as! [NSManagedObject] {
                
                print(data.value(forKey: "id") as! String)
                let edo = data.value(forKey: "nombre") as! String
                
                arrEdos.append(edo)
                arrEdos = arrEdos.sorted(by: <)
            }
        } catch {
            print("Failed")
        }
        return arrEdos
    }
    
    class func arrCategory() -> [String] {
        let context = CD.share.context
        
        var arrCate = [String]()
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Categorias")
        //request.predicate = NSPredicate(format: "Categorias = %@", estado)
        request.returnsObjectsAsFaults = false
        do {
            let result = try context.fetch(request)
            for data in result as! [NSManagedObject] {
                
                print(data.value(forKey: "id") as? Int ?? 0)
                let nombre = data.value(forKey: "nombre") as? String ?? String.Empty
                arrCate.append(nombre)
                arrCate = arrCate.sorted(by: <)
            }
        } catch {
            print("Failed")
        }
        return arrCate
    }
    
    class func category(tiendaId: Int) -> String {
        let context = CD.share.context
        
        var tienda = String.Empty
//        var arrCate = [String]()
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Tiendas")
        request.predicate = NSPredicate(format: "id = %d", tiendaId)
        request.returnsObjectsAsFaults = false
        do {
            let result = try context.fetch(request) as! [NSManagedObject]
            let e = result.first
            tienda = e?.value(forKey: "nombre") as? String ?? String.Empty
//            for data in result as! [NSManagedObject] {
//
//                let nombre = data.value(forKey: "nombre") as? String ?? String.Empty
//                arrCate.append(nombre)
//            }
        } catch {
            print("Failed")
        }
        return tienda
    }
    
    class func getIdEdoFrom(nombreEdo: String) -> Int {
        let context = CD.share.context
        
        var edoId = 1
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Estados")
        request.predicate = NSPredicate(format: "nombre = %@", nombreEdo)
        request.returnsObjectsAsFaults = false
        do {
            let result = try context.fetch(request) as! [NSManagedObject]
            if let e = result.first {
                edoId = Int(e.value(forKey: "id") as? String ?? String.Empty) ?? 1
            }
        } catch {
            print("Failed")
        }
        return edoId
    }
    
    class func getIdCtgryFrom(nombreCtgry: String) -> Int {
        let context = CD.share.context
        
        var ctgryId = 1
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Categorias")
        request.predicate = NSPredicate(format: "nombre = %@", nombreCtgry)
        request.returnsObjectsAsFaults = false
        do {
            let result = try context.fetch(request) as! [NSManagedObject]
            let e = result.first
            ctgryId = e?.value(forKey: "id") as? Int ?? 0
        } catch {
            print("Failed")
        }
        return ctgryId
    }
    
}
