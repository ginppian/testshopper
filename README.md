Test Shopper
===

## 1

1.1 Consumir el siguiente texto con un json <a href="http://uneabastomovil.com/services/jsonPrueba.txt">jsonPrueba.txt</a>
<br>

1.2 Con la información parseada del json:

1.2.1 Mostrar una pantalla con los siguientes componentes:
               
1.3 Parte superior con un UISegmentedControl con opciones: "Estados" y "Categorias"

1.3.1 Resto de la pantalla mostrar lista de Estados o Categorias(Que se obtuvieron del json)
               
1.4 Al seleccionar un item mostrar:
               
1.4.1 Listado con los comercios correspondientes con los datos: nombre, estado y categoria
               
* Hacer uso de coredata, autolayout

## 2

Hacer un playground que resuelva lo siguiente:
               
Odómetro descompuesto.
A un odómetro le falta el digito 4, es decir del 3 pasa al 5, del 13 al 15, 39 al 50 y así sucesivamente. Dado un numero mayor a 0 y menor o igual a 1 000 000 imprimir el numero real recorrido
               
               Ej 1:
               Entrada:
               9
               Salida
               8
               
               Ej 2:
               Entrada:
               12356
               Salida
               8303