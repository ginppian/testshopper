import UIKit
import Foundation

func odometro(_n: Int) -> Int {
    var n = _n
    var cnt = 0
    while n > 0 {
        let s = String(n)
        if s.contains("4") {
            cnt += 1
        }
        n -= 1
    }
    return _n - cnt
}

let diff = odometro(_n: 12356)
print(diff)
